# Startup poc project front
## Installation
Réaliser les étapes suivantes dans le dossier du projet : 
1. `yarn install`
2. `npm install moment --save`
3. `yarn start`

## Fonctionnalités
L'application est composée d'une page `home` donnant accès à la page d'authentification via le bouton `Sign in`.  Cette page est nommée `login` et permet à l'utilisateur d'accéder à son espace suivant ces droits et seulement si l'authentification est validée.
Les espaces sont les suivants:
 1. L'espace **développeur** dans lequel il est possible d'éditer son rapport mensuel ou entre deux dates et de saisir ses temps sur une Milestone correspondant à un projet.
 2. L'espace **manager** où celui-ci peut ajouter des développeurs parmi ceux disponibles (pas encore affectés), supprimer des développeurs de sa liste, afficher les détails d'un utilisateur et éditer le rapport mensuel ou entre deux dates de l'un de ses développeurs.
 3. L'espace **admin** dans lequel il pourra ajouter un utilisateur dans la base, afficher les détails des utilisateurs, changer le rôle de l'utilisateur ainsi que son manager et supprimer cet utilisateur de l'application.

De manière plus générale, dans les trois espaces, l'utilisateur a accès aux détails de son profil : (login, nom, prénom, ...) et la possibilité de se déconnecter.

## Organisation des services
Dans le projet, cinq services sont utilisés:
- `login.service` assurant l'authentification
- `logs.service` correspondant aux opérations sur les logs
- `milestone.service` pour gérer les appels api associés aux Milestones
- `project.service` correspondant aux projets
- `user.service` réalisant tous les appels api pour les personnes

## Organisation des pages
 - `home` est la page d'accueil de notre application
 - `login` permet l'authentification et la redirection d'un utilisateur sur le bon espace
 - `admin-tabs`, `manager-tabs`, `user-tabs` permettent de gérer les différents tabs des 3 espaces
 - `admin-tab-home`, `manager-tab-home`, `user-tab-home` sont les pages d'accueil des utilisateurs avec la possibilité de se déconnecter. Elles utilisent toutes les trois le composant `home-details`.
 - `admin-tab-users` et `manager-tab-users` sont les pages de l'espace admin et manager pour la gestion des utilisateurs (différent suivant les droits). Dans celles-ci, le component `user-list` est utilisé et les pages sont associées aux components `insert-user` et `edit-report` respectivement pour insérer un utilisateur et éditer le rapport entre deux dates, ainsi que les pages `admin-tab-users-details` et `manager-tab-users-details` pour effectuer des opérations relatives à des utilisateurs et afficher les détails d'un utilisateur.
 - `user-tab-projects` et `manager-tab-projects` proposent la partie gestion des projets pour le développeur et le manager. Dans celles-ci, les components `project-list`  et `log-list` sont utilisés. Elles sont également associées aux pages `log-details` , `milestone-select` et `add-project` pour assurer les fonctionnalités correspondantes. De plus, la visualisation des détails d'un projet est assurée par la page `project-details`
