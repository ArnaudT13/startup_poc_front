import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
/**
 * Service to retrieve data from Milestone Table through the API.
 */
export class MilestoneService {

  constructor(
    public httpApi: HttpClient
  ) { }

  /**
   * Retrieve the milestone by using the milstone Id 
   * @param milestoneId The id of the milestone
   * @returns The observable milestone 
   */
  getMilestoneByMilestoneId(milestoneId: number) {
    return this.httpApi.get('http://localhost:8180/milestones/' + milestoneId).pipe(
      map((response: any) => response),
    );
  }
}
