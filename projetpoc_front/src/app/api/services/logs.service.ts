import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
/**
 * Service to retrieve data from Logs Table through the API.
 */
export class LogsService {

  constructor(
    public httpApi: HttpClient
  ) { }

  /**
   * Retrieve a log by using its id
   * @param logId The id of the log
   * @returns The observable Log 
   */
  getLogById(logId: number) {
    return this.httpApi.get('http://localhost:8180/logs/' + logId).pipe(
      map((response: any) => response),
    );
  }

  /**
   * Retrieve the milestone of a log by using its Id
   * @param logId The id of the log
   * @returns The observable milestone 
   */
  getMilestoneByLogId(logId: number) {
    return this.httpApi.get('http://localhost:8180/logs/' + logId + '/milestone').pipe(
      map((response: any) => response),
    );
  }

  /**
   * Retrieve all the logs of a specified project for a particular person.
   * @param projectId The id of the project
   * @param personId The id of the person
   * @returns The observable list of logs.
   */
  getLogsForPersonProject(projectId: number, personId: number) {
    return this.httpApi.get('http://localhost:8180/logs/projects/' + projectId + '/person/' + personId).pipe(
      map((response: any) => response),
    );
  }

    /**
   * Get all logs for a project in database
   * @param projectId The id of the project
   * @returns The observable list of logs.
   */
  getLogsForProject(projectId: number){
    return this.httpApi.get('http://localhost:8180/logs/projects/' + projectId).pipe(
      map((response: any) => response),
    );
  }

  /**
   * Add a log in database
   * @param logEditDate The editDate of the log
   * @param logGranularity The granularity of the log
   * @param logDescription The description of the log
   * @param milestone The milestone
   * @param person The person
   * @returns The result of the operation
   */
  addLog(logEditDate: string, logGranularity: number, logDescription: string, milestoneId: number, personId: number): Promise<boolean>{
    let headers = new HttpHeaders();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    const requestOptions = { headers: headers };

    let patchData = {   
      "id":null,
      "editDate": logEditDate,
      "granularity": logGranularity,
      "description": logDescription
    }

    console.log(patchData);
    console.log(milestoneId);
    return new Promise(resolve => {
        this.httpApi.patch("http://localhost:8180/milestones/" + milestoneId + "/persons/" + personId  + "/logs/add", patchData, requestOptions)
        .subscribe(data => {
          console.log(data);
          resolve(true);
        }, error => {
          resolve(false);
          console.log(error);
      });
    });

  }

  /**
   * Update a log in database
   * @param logId The id of the log
   * @param logEditDate The editDate of the log
   * @param logGranularity The granularity of the log
   * @param logDescription The description of the log
   * @returns The result of the operation
   */
  updateLog(logId: number, logEditDate: string, logGranularity: number, logDescription: string): Promise<boolean>{
    let headers = new HttpHeaders();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    const requestOptions = { headers: headers };

    let patchData = {   
      "id":logId,
      "editDate": logEditDate,
      "granularity": logGranularity,
      "description": logDescription
    }

    return new Promise(resolve => {
        this.httpApi.patch("http://localhost:8180/logs/" + logId + "/modif/", patchData, requestOptions)
        .subscribe(data => {
          console.log(data);
          resolve(true);
        }, error => {
          resolve(false);
          console.log(error);
      });
    });

  }

  /**
   * Delete a log by its id.
   * @param logId The id of the log to be deleted
   * @returns The observable list of logs.
   */
  deleteLog(logId: number): boolean {
    let headers = new HttpHeaders();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    const requestOptions = { headers: headers };

    let status;
    this.httpApi.delete('http://localhost:8180/logs/remove/' + logId, requestOptions) // TODO set url de la modif du manager
    .subscribe(
      response => {
        console.log(response);
        status = true;
      },
      error => {
        status = false;
        console.log(error);
      });
    return status;
  }
}
