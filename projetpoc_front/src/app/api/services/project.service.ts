import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
/**
 * Service to retrieve data from Project Table through the API.
 */
export class ProjectService {

  constructor(
    public httpApi: HttpClient
  ) { }

  /**
   * Retrieve the project list of a user
   * @param userId The id of the user
   * @returns The observable project list 
   */
  getUserProjectList(userId: number) {
    return this.httpApi.get('http://localhost:8180/persons/' + userId + '/projects').pipe(
      map((response: any) => response),
    );
  }

  /**
   * Retrieve the project details
   * @param projectId The id of the project
   * @returns The observable project details 
   */
  getProjectDetails(projectId: number) {
    return this.httpApi.get('http://localhost:8180/projects/' + projectId).pipe(
      map((response: any) => response),
    );
  }

  /**
   * Retrieve a list of project with same names
   * @param projectName The name of the project
   * @returns The observable project lists 
   */
  getProjectByName(projectName: string) {
    let headers = new HttpHeaders();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    const requestOptions = { headers: headers };

    return this.httpApi.get("http://localhost:8180/projects/name?name=" + projectName, requestOptions).pipe(
          map((response: any) => response),
        );
  }

  /**
   * Add project in database
   * @param name The name of the project
   * @param milestoneDescription description of the milestone
   * @param milestoneStartDate Starting date of the milestone
   * @param milestoneEndDate Ending date of the milestone
   * @returns The result of the operation
   */
  createProject(name: string, milestoneDescription: string, milestoneStartDate: string, milestoneEndDate: string): Promise<boolean>{
    let headers = new HttpHeaders();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    const requestOptions = { headers: headers };

    let postData = {
      "id": 0,
      "milestones": [
        {
          "description": milestoneDescription,
          "endDate": milestoneEndDate,
          "id": 0,
          "milestoneTitle": {
            "id": 1,
            "title": "Initialisation"
          },
          "startDate": milestoneStartDate
        }
      ],
      "name": name
    }

    return new Promise(resolve => {
        this.httpApi.post("http://localhost:8180/project/add", postData, requestOptions)
        .subscribe(data => {
          resolve(true);
        }, error => {
          resolve(false);
          console.log(error);
      });
    });

  }

  /**
   * Add a contributor to a project
   * @param projectId The id of the project
   * @param contributorId The id of the contributor
   * @returns The result of the operation
   */
  addContributorToProject(projectId: number, contributorId: number): Promise<boolean>{
    let headers = new HttpHeaders();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    const requestOptions = { headers: headers };

    return new Promise(resolve => {
        this.httpApi.patch("http://localhost:8180/projects/" + projectId + "/add_contributor/" + contributorId, requestOptions)
        .subscribe(data => {
          console.log(data);
          resolve(true);
        }, error => {
          resolve(false);
          console.log(error);
      });
    });

  }
}
