import { HttpClient,  HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import {forkJoin, from, Observable, Subject} from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  userListSubject = new Subject<any[]>();
  userList = [];
  constructor(
    public httpApi: HttpClient,
  ) { }

  emitUserListSubject(){
    this.userListSubject.next(this.userList.slice());
  }
  /**
   * Retrieve the person list of a manager
   * @param managerId The id of the manager
   * @returns The observable user list 
   */
  getUserListForAManager(managerId: number) : Observable<any> {
    return this.httpApi.get(`http://localhost:8180/persons/${managerId}/developers`).pipe(
      map((response: any) => response)
    );
  }

  /**
   * Retrieve the person details of a user
   * @param userId The user id
   * @returns The details of the user
   */
  getUserDetail(userId: number): Observable<any> {
    return this.httpApi.get(`http://localhost:8180/person/${userId}`).pipe(
      map((response: any) => response)
    );
  }

  /**
   * Retrieve the projects related to the user
   * @param userId The user id
   * @returns The projects of the user
   */
  getUserProjects(userId: number): Observable<any> {
    return this.httpApi.get(`http://localhost:8180/persons/${userId}/projects`).pipe(
      map((response: any) => response)
    );
  }
  
  /**
   * Retrieve the projects related to the user
   * @param projectId The project id
   * @returns The projects of the user
   */
  getUserProjectsSub(projectId: number) {
    this.httpApi.get(`http://localhost:8180/projects/${projectId}/persons`).subscribe(
      (response: any) => {
        this.userList = response;
        this.emitUserListSubject();
      },
      (error) => {
        console.log('Erreur ! : ' + error);
      }
  );
  }

  /**
   * Retrieve the logs list of a user
   * @param userId The id of the user
   * @returns The logs list
   */
  getUserLogs(userId: number): Observable<any> {
    return this.httpApi.get(`http://localhost:8180/logs/person/${userId}`).pipe(
      switchMap((logs: any[]) => {
        return forkJoin(
          logs.map((log: any) => {
            return this.httpApi.get(`http://localhost:8180/logs/${log.id}/project`).pipe(
              map((responseLog: any) => {
                log.project = responseLog.name;
                return log;
              })
            )
          })
        );
      }));
  }

    /**
   * Retrieve the user list of managers
   * @param managerId The id of the manager
   * @returns The user list
   */
  getUsersOfManager(managerId: number): Observable<any> {
    return this.httpApi.get("http://localhost:8180/persons/" + managerId + "/developers").pipe(
      map((response: any) => response)
    );
  }

  /**
   * Retrieve the user list
   * @returns The user list
   */
  getUsers(): Observable<any> {
    return this.httpApi.get('http://localhost:8180/persons').pipe(
      map((response: any) => response)
    );
  }

  /**
   * Retrieve the user list subject
   */
  getUsersSub() {
    this.httpApi.get<any[]>('http://localhost:8180/persons')
        .subscribe(
            (response) => {
              this.userList = response;
              this.emitUserListSubject();
            },
            (error) => {
              console.log('Erreur ! : ' + error);
            }
        );
  }

  /**
   * Retrieve the user list subject
   */
  getUsersForManagerSub(managerId: number) {
    this.httpApi.get<any[]>(`http://localhost:8180/persons/${managerId}/developers`)
        .subscribe(
            (response) => {
              this.userList = response;
              this.emitUserListSubject();
            },
            (error) => {
            }
        );
  }
  
  /**
   * Retrieve the project of a log
   * @param logId The log Id
   * @returns The project to retrieve
   */
  getProjectByLog(logId : number): Observable<any> {
    return this.httpApi.get(`http://localhost:8180/logs/${logId}/project`).pipe(
      map((response: any) => response)
    );
  }

  /**
   * Change or add a manager to the user
   * @param userId The user id
   * @param managerId The manager id
   * @returns The response to the operation
   */
  addOrChangeManagerForUser(userId: number, managerId: number): Promise<boolean> {
    return new Promise(resolve => {
      this.httpApi.patch(`http://localhost:8180/person/${userId}/manager/${managerId}`, null)
        .subscribe(
            () => {
              this.getUsersForManagerSub(managerId);
            resolve(true);
        },  () => {
            resolve(false);
        });
    });
  }

  /**
   * Change user role
   * @param userId The user id
   * @param managerId The role id
   * @returns The response to the operation
   */
  changeRoleForUser(userId: number, roleId: number): Promise<boolean> {
    return new Promise(resolve => {
      this.httpApi.patch(`http://localhost:8180/person/${userId}/role/${roleId}`, null)
        .subscribe(
            () => {
              this.getUsersSub();
            resolve(true);
        },  () => {
            resolve(false);
        });
    });
  }

  /**
   * Retrieve the person related to the username
   * @param username The username of the user
   * @returns The user corresponding to the username
   */
  getUserByUsername(username: String): Observable<any>{
    return this.httpApi.get(`http://localhost:8180/person/username?username=${username}`).pipe(
      map((response: any) => response)
    );
  }

  /**
   * Delete manager to the user
   * @param userId The user Id
   * @returns The response to the operation
   */
  deleteUserForManager(userId: number,managerId: number): Promise<boolean> {
    return new Promise(resolve => {
      this.httpApi.patch(`http://localhost:8180/person/${userId}/remove_manager`, null)
        .subscribe(
            () => {
              this.getUsersForManagerSub(managerId);
            resolve(true);
        },  () => {
            resolve(false);
        });
    });
  }

  /**
   * Add user in database
   * @param firstName The firstname of the user
   * @param lastName The lastname of the user
   * @param login The login of the user
   * @param password The password of the user
   * @param role The role of the user
   * @returns The result of the operation
   */
  addUser(firstName: string, lastName: string, login: string, password: string, role: number): Promise<boolean>{
    let headers = new HttpHeaders();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    const requestOptions = { headers: headers };

    let postData = {   
      "id":null,
      "firstName": firstName,
      "lastName": lastName,
      "login": login,
      "role":{
          "id": role
      },
      "password": password,
      "projects": [],
      "manager": null
    }

    return new Promise(resolve => {
        this.httpApi.post("http://localhost:8180/person/add", postData, requestOptions)
        .subscribe(
            () => {
              this.getUsersSub();
              resolve(true);
        }, error => {
          resolve(false);
          console.log(error);
      });
    });

  }

  /**
   * Delete user in database
   * @param userId The user id
   */
  deleteUser(userId: number) : Promise<boolean>{
    return new Promise(resolve => {
      this.httpApi.patch(`http://localhost:8180/person/${userId}/remove`, null)
        .subscribe(
            () => {
              this.getUsersSub();
              resolve(true);
        },  (error) => {
            resolve(false);
            console.log(error);
        });
    });
  }

  /**
   * Edit the report between two dates of a user
   * @param userId The user Id
   * @param startDate The start date of the report
   * @param endDate The end date of the report
   */
  editReportBetweenTwoDates(userId: number, startDate: Date, endDate: Date)  {
    // Get URL
    const url = `http://localhost:8180/persons/${userId}/report/${startDate}/${endDate}`;
    // Open report in a new window
    window.open(url, '_system')
  }

  /**
   * Edit the month report of a user
   * @param userId The user Id
   */
  editMonthReport(userId: number){
    var d = new Date();
    let m = d.getMonth(); //current month
    let y = d.getFullYear(); //current year
    let startDate = new Date(y,m,2).toJSON().substring(0,10); //this is first day of current month
    let endDate = new Date(y,m+1,1).toJSON().substring(0,10); //this is last day of current month   
    
    // Get URL
    const url = `http://localhost:8180/persons/${userId}/report/${startDate}/${endDate}`;
    // Open report in a new window
    window.open(url, '_system')
  }


  /**
   * Function used to display the toast of an operation sucess or error
   * @param toastCtrl The ToastControler
   * @param status The boolean status : True --> success, False --> error
   * @param successMesage The success message
   * @param errorMessage The error message
   */
  async manageSuccessErrorToast(toastCtrl : ToastController, status: boolean, successMesage : String, errorMessage : String): Promise<void> {
    let toast;
    if (status === true) {
      toast = await toastCtrl.create({
        message: 'Success : ' + String(successMesage),
        color: 'success',
        translucent: true,
        duration: 3000
      });
    }else{
      toast = await toastCtrl.create({
        message: 'Error : ' + String(errorMessage),
        color: 'danger',
        translucent: true,
        duration: 3000
      });
    }

    await toast.present();
  }

  /**
   * Fonction used to display toast info
   * @param toastCtrl The Toast Controller
   * @param infoMesage The message to display
   */
  async manageInfoToast(toastCtrl : ToastController, infoMesage : String) {

    const toast = await toastCtrl.create({
        message: String(infoMesage),
        translucent: true,
        duration: 3000
      });

    toast.present();
  }

  /**
   * Fonction used to display toast eror
   * @param toastCtrl The Toast Controller
   * @param infoMesage The message to display
   */
  async manageErrorToast(toastCtrl : ToastController, errorMesage : String) {

    const toast = await toastCtrl.create({
        message: String(errorMesage),
        color: 'danger',
        translucent: true,
        duration: 3000
      });

    toast.present();
  }

  /**
   * Retrieve the role list
   * @returns The role list
   */
  getRoles(): Observable<any> {
    return this.httpApi.get('http://localhost:8180/roles').pipe(
      map((response: any) => response)
    );
  }  
}
