import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, ToastController } from '@ionic/angular';
import { finalize } from 'rxjs/operators';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private sessionUser: any;

  authenticated: boolean;

  constructor(
    public http : HttpClient,
    public router : Router,
    public userService : UserService
  ) { }

  /**
   * Function used to authenticate the user
   * @param credentials The credentials of the user
   * @param callback
   * @param toastCtrl The toast controller
   */
  authenticate(credentials, callback, toastCtrl: ToastController) {
    const headers = new HttpHeaders(credentials ? {
        authorization : 'Basic ' + btoa(credentials.username + ':' + credentials.password)
    } : {});

    this.http.get('http://localhost:8180/user', {headers: headers}).subscribe(response => {
        if (response['name']) {
            this.authenticated = true;
            // Token exist
            localStorage.setItem("tokenExist", String(true));
        } else {
            this.authenticated = false;
        }
        return callback && callback();
    },
    () => {
      this.userService.manageErrorToast(toastCtrl, "Login or password incorrect");
    });

  }

  getSessionUser() : any {
    return this.sessionUser;
  }

  setSessionUser(user : any) {
    this.sessionUser = user;
  }

  /**
   * Use to manage the logout operation
   * @returns
   */
  logout() {
    this.http.post('http://localhost:8180/logout', {}).pipe(
      finalize(() => {
          this.authenticated = false;
          this.router.navigateByUrl('/login');
      })).subscribe();
    sessionStorage.removeItem('userId');
    sessionStorage.removeItem('role');
    sessionStorage.removeItem('insertCreator');
    localStorage.removeItem("tokenExist");
  }

  
}
