/**
 * Interface for User
 */
export interface User {
    id: number;
    firstName: string;
    lastName: string;
}
