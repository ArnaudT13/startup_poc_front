import { ElementArrayFinder } from "protractor";
/**
 * Interface for Project
 */
export interface Project {
    id: number;
    name: string;
  }
  