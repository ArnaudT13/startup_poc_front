import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {UserService} from "../../../api/services/user.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
})
/**
 * Component displaying a list of users and sending the user Id upon click.
 */
export class UserListComponent implements OnInit,OnDestroy {
  userList$ = [];
  userServiceSub = new Subscription();

  @Input()
  userId : number = -1;
  @Output()
  selectedUser = new EventEmitter<number>();

  constructor(private userService : UserService) { }

  ngOnInit() {
    // SUBSCRIPTION: Will be refreshed whenever userService.emitUserListSubject(); will be called.
    this.userServiceSub = this.userService.userListSubject.subscribe(
        (users : any[])=>{
          this.userList$ = users.filter(value => {console.log(value);return value.id != this.userId});
          console.log(this.userList$);
        }
    )
    this.userService.emitUserListSubject();
  }
  
  ngOnDestroy() {
    this.userServiceSub.unsubscribe();
  }

}
