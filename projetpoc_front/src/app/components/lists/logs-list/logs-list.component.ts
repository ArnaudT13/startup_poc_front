import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-logs-list',
  templateUrl: './logs-list.component.html',
  styleUrls: ['./logs-list.component.scss'],
})
/**
 * Component displaying a list of logs and sending the log Id upon click.
 */
export class LogsListComponent implements OnInit {
  
  @Input()
  logs$: any;

  @Output()
  logId = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {}

}
