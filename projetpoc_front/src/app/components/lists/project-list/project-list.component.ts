import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * Component displaying a list of projects and sending the project Id upon click.
 */
export class ProjectListComponent implements OnInit {

  @Input()
  projectList: any;

  @Output()
  selectedProject = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {}

}
