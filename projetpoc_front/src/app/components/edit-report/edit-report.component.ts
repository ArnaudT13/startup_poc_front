import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { UserService } from 'src/app/api/services/user.service';

@Component({
  selector: 'app-edit-report',
  templateUrl: './edit-report.component.html',
  styleUrls: ['./edit-report.component.scss'],
})
export class EditReportComponent implements OnInit {

  private responseEditingOperation: Promise<boolean>;

  private insertCreator$ : String;

  userForm: FormGroup;

  constructor(
    public formBuilder: FormBuilder,
    public userService: UserService,
    public toastCtrl : ToastController,
    public router: Router
    ) { }

  ngOnInit() {
    this.initForm();
  }

  /**
   * Init the insert form
   */
  initForm() {
    this.userForm = this.formBuilder.group({
      startDate: ['', [Validators.required]],
      endDate: ['', [Validators.required]]
    });
    this.insertCreator$ = String(sessionStorage.getItem('insertCreator'));
  }

  /**
   * Edit the report of the user
   */
  onSubmitForm() {
    if(!this.userForm.valid){
      this.userService.manageErrorToast(this.toastCtrl, "Error : Select dates");
    }else{
      const formValue = this.userForm.value;

      this.userService.editReportBetweenTwoDates(Number(sessionStorage.getItem('selectedUserIdForComponent')), formValue['startDate'], formValue['endDate']);
      
      if(this.insertCreator$ === "fromUser"){
        this.router.navigateByUrl('user-tabs/user-tab-home');
      }else if(this.insertCreator$ === "fromManager"){
        this.router.navigateByUrl('manager-tabs/manager-tab-users');
      }
    }
  }

}
