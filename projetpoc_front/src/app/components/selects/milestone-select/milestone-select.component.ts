import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-milestone-select',
  templateUrl: './milestone-select.component.html',
  styleUrls: ['./milestone-select.component.scss'],
})
/**
 * Component allowing selection between milestones in a milestone array given in input and sending the milestone Id upon click.
 */
export class MilestoneSelectComponent implements OnInit {

  private milestoneSelect: any;
  
  @Input()
  milestones$: any;
  @Input()
  latestMilestoneId$: any;

  @Output()
  milestoneId = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }
  
  public milestoneChanged(): void { 
    this.milestoneId.emit(this.milestoneSelect);
  }

}
