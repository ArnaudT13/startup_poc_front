import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { CustomValidator } from 'src/app/api/class/custom-validator';
import { UserService } from 'src/app/api/services/user.service';

@Component({
  selector: 'app-insert-user',
  templateUrl: './insert-user.component.html',
  styleUrls: ['./insert-user.component.scss'],
})
export class InsertUserComponent implements OnInit {

  private responseAddingOperation: Promise<boolean>;

  userForm: FormGroup;

  roleList$: Observable<any>;

  constructor(
    public formBuilder: FormBuilder,
    public userService: UserService,
    public toastCtrl : ToastController,
    public router: Router
  ) { }

  ngOnInit() {
    this.initForm();
    this.roleList$ = this.userService.getRoles();
  }


  /**
   * Init the insert form
   */
  initForm() {
    this.userForm = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      login: ['', [Validators.required]],
      password: [ '', Validators.compose([ 
        Validators.required,
        CustomValidator.patternValidator(/\d/, { hasNumber: true }),
        CustomValidator.patternValidator(/[A-Z]/, { hasCapitalCase: true }),
        CustomValidator.patternValidator(/[a-z]/, { hasSmallCase: true }),
        CustomValidator.patternValidator(/[@#$\^%&]/, { hasSpecialCharacters: true }),
        Validators.minLength(8)])
     ],
     confirmPassword: ['', [Validators.required]],
     role: ['', [Validators.required]],
    },
    {
       // check whether if password and confirm password match
       validators: CustomValidator.checkPasswords
    });
  }

  /**
   *  Function related to the insert form button
   */
  onSubmitForm () {
    console.log(this.userForm.controls['firstName'].valid);
    if (!this.userForm.valid) {
      if(!this.userForm.controls['firstName'].valid){
        this.userService.manageErrorToast(this.toastCtrl, "Firstname incorrect")
      }else if(!this.userForm.controls['lastName'].valid){
        this.userService.manageErrorToast(this.toastCtrl, "Lastname incorrect")
      }else if(!this.userForm.controls['login'].valid){
        this.userService.manageErrorToast(this.toastCtrl, "Login incorrect")
      }else if(!this.userForm.controls['password'].valid){
        this.userService.manageErrorToast(this.toastCtrl, "Password incorrect")
      }else if(!this.userForm.controls['confirmPassword'].valid){
        this.userService.manageErrorToast(this.toastCtrl, "Confirm Password incorrect")
      }else if(!this.userForm.controls['role'].valid){
        this.userService.manageErrorToast(this.toastCtrl, "Role incorrect")
      }else{
        this.userService.manageErrorToast(this.toastCtrl, "Password and Confirm Password not match")
      }
    } else {
      const formValue = this.userForm.value;
      this.responseAddingOperation = this.userService.addUser(
        formValue['firstName'],
        formValue['lastName'],
        formValue['login'],
        formValue['password'],
        formValue['role']);

      this.responseAddingOperation
        .then((response) => {
            this.userService.manageSuccessErrorToast(this.toastCtrl, response, "User added", "User not added");
          });
      this.router.navigateByUrl('admin-tabs/admin-tab-users', {skipLocationChange: true});
    }
  }
}
