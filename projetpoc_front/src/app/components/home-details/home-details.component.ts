import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from 'src/app/api/services/user.service';

@Component({
  selector: 'app-home-details',
  templateUrl: './home-details.component.html',
  styleUrls: ['./home-details.component.scss'],
})
export class HomeDetailsComponent implements OnInit {

  private userId: number;
  private role$: string;

  private styleDivider$: any;
  private styleIconUser$: any;

  userDetail$: Observable<any>;
  userProjects$: Observable<any>;
  userLogs$: Observable<any>;
  


  constructor(
    public userService: UserService,
    public router: Router
  ) { }



  ngOnInit() {
    this.userId = Number(sessionStorage.getItem("userId"));

    this.role$ = String(sessionStorage.getItem("role"));

    this.userDetail$ = this.userService.getUserDetail(this.userId);

    this.userProjects$ = this.userService.getUserProjects(this.userId);

    this.userLogs$ = this.userService.getUserLogs(this.userId);

    this.setStyle();
  }

  /**
   * Set the style of the divider home page
   */
  setStyle(){
    if(this.role$ == 'developer'){
      this.styleDivider$ = {'background-color': '#51ADF9'};
      this.styleIconUser$ = {'color': '#0C5FA4'};
    }else if(this.role$ == 'manager'){
      this.styleDivider$ = {'background-color': '#7CB485'};
      this.styleIconUser$ = {'color': '#435646'};
    }else if(this.role$ == 'admin'){
      this.styleDivider$ = {'background-color': '#CB8282'};
      this.styleIconUser$ = {'color': '#994747'};
    }else{
      this.styleDivider$ = {'background-color': '#51ADF9'};
      this.styleIconUser$ = {'color': '#0C5FA4'};
    }
    
  }
}
