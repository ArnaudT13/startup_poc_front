import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { start } from 'repl';
import { EditReportComponent } from './components/edit-report/edit-report.component';
import { InsertUserComponent } from './components/insert-user/insert-user.component';
import { IsSignedInGuard } from './guard/is-sign-in.guard';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'offline',
    loadChildren: () => import('./pages/offline/offline.module').then( m => m.OfflinePageModule)
  },
  {
    path: 'user-tabs',
    loadChildren: () => import('./pages/user-tabs/user-tabs.module').then( m => m.UserTabsPageModule),
    canActivate: [IsSignedInGuard]
  },
  {
    path: 'manager-tabs',
    loadChildren: () => import('./pages/manager-tabs/manager-tabs.module').then( m => m.ManagerTabsPageModule),
    canActivate: [IsSignedInGuard]
  },
  {
    path: 'admin-tabs',
    loadChildren: () => import('./pages/admin-tabs/admin-tabs.module').then( m => m.AdminTabsPageModule),
    canActivate: [IsSignedInGuard]
  },
  {
    path: 'manager-tab-users-details',
    loadChildren: () => import('./pages/manager-tab-users-details/manager-tab-users-details.module').then( m => m.ManagerTabUsersDetailsPageModule),
    canActivate: [IsSignedInGuard]
  },
  {
    path: 'admin-tab-users-details',
    loadChildren: () => import('./pages/admin-tab-users-details/admin-tab-users-details.module').then( m => m.AdminTabUsersDetailsPageModule),
    canActivate: [IsSignedInGuard]
  },
  {
    path: 'insert-user',
    component: InsertUserComponent,
    canActivate: [IsSignedInGuard]
  },
  {
    path: 'edit-report',
    component: EditReportComponent,
    canActivate: [IsSignedInGuard]
  }, {
    path: 'project-details',
    loadChildren: () => import('./pages/project-details/project-details.module').then( m => m.ProjectDetailsPageModule),
    canActivate: [IsSignedInGuard]
  },
  {
    path: 'log-details',
    loadChildren: () => import('./pages/log-details/log-details.module').then( m => m.LogDetailsPageModule),
    canActivate: [IsSignedInGuard]
  },
  {
    path: 'add-project',
    loadChildren: () => import('./pages/add-project/add-project.module').then( m => m.AddProjectPageModule),
    canActivate: [IsSignedInGuard]
  },
  {
    path: '',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },

];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}