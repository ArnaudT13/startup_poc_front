import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdminTabUsersPageRoutingModule } from './admin-tab-users-routing.module';

import { AdminTabUsersPage } from './admin-tab-users.page';
import { UserListComponent } from '../../components/lists/user-list/user-list.component';
import { InsertUserComponent } from 'src/app/components/insert-user/insert-user.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdminTabUsersPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [AdminTabUsersPage, UserListComponent, InsertUserComponent]
})
export class AdminTabUsersPageModule {}
