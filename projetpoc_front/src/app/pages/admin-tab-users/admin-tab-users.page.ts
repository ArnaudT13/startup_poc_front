import {Component, OnDestroy, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, ToastController } from '@ionic/angular';
import {from, Observable, pipe, Subscription} from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';
import { LoginService } from '../../api/services/login.service';
import { UserService } from '../../api/services/user.service';

@Component({
  selector: 'app-admin-tab-users',
  templateUrl: './admin-tab-users.page.html',
  styleUrls: ['./admin-tab-users.page.scss'],
})
export class AdminTabUsersPage implements OnInit{

  private adminId: number;
  private tabTitle$: string;

  constructor(
    public router: Router,
    public userService: UserService,
    public loginService: LoginService,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController
  ) { }

  ngOnInit() {
    this.adminId = Number(sessionStorage.getItem("userId"));
    this.userService.getUsersSub();
    this.userService.getUserDetail(this.adminId).subscribe(user => {
      this.tabTitle$ =  user.firstName + " " + user.lastName + " : " + user.role.label;
    });
  }

  /**
   * Select a user in the list and route to the user-datails page
   * @param id The user id
   */
  async selectedUser(id: number) {
    sessionStorage.setItem('userSelectedId', String(id));
    await this.router.navigateByUrl('admin-tab-users-details');
  }

  /**
   * Add user in database
   */
  addUser() {
    this.router.navigateByUrl('insert-user');
  }
}
