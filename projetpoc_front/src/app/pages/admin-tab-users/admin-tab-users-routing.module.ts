import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InsertUserComponent } from 'src/app/components/insert-user/insert-user.component';

import { AdminTabUsersPage } from './admin-tab-users.page';

const routes: Routes = [
  {
    path: '',
    component: AdminTabUsersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminTabUsersPageRoutingModule {}
