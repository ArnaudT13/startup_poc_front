import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminTabHomePage } from './admin-tab-home.page';

const routes: Routes = [
  {
    path: '',
    component: AdminTabHomePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminTabHomePageRoutingModule {}
