import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../api/services/login.service';
import { UserService } from '../../api/services/user.service';

@Component({
  selector: 'app-admin-tab-home',
  templateUrl: './admin-tab-home.page.html',
  styleUrls: ['./admin-tab-home.page.scss'],
})
export class AdminTabHomePage implements OnInit {

  private adminId: number;
  private tabTitle$: string;
  
  constructor(
    public loginService : LoginService,
    public userService : UserService
    ) { }

  ngOnInit() {
    this.adminId = Number(sessionStorage.getItem("userId"));
    this.userService.getUserDetail(this.adminId).subscribe(user => {
      this.tabTitle$ =  user.firstName + " " + user.lastName + " : " + user.role.label;
    });
  }

  logoutFromSession(){
    this.loginService.logout();
  }

}
