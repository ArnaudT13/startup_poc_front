import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdminTabHomePageRoutingModule } from './admin-tab-home-routing.module';

import { AdminTabHomePage } from './admin-tab-home.page';
import { HomeDetailsComponent } from 'src/app/components/home-details/home-details.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdminTabHomePageRoutingModule
  ],
  declarations: [AdminTabHomePage, HomeDetailsComponent]
})
export class AdminTabHomePageModule {}
