import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AdminTabHomePage } from './admin-tab-home.page';

describe('AdminTabHomePage', () => {
  let component: AdminTabHomePage;
  let fixture: ComponentFixture<AdminTabHomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminTabHomePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AdminTabHomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
