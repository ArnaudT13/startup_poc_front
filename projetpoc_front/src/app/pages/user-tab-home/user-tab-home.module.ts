import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UserTabHomePageRoutingModule } from './user-tab-home-routing.module';

import { UserTabHomePage } from './user-tab-home.page';
import { HomeDetailsComponent } from 'src/app/components/home-details/home-details.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UserTabHomePageRoutingModule
  ],
  declarations: [UserTabHomePage, HomeDetailsComponent]
})
export class UserTabHomePageModule {}
