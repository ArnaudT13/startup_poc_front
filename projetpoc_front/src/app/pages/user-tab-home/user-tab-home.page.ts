import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../../api/services/login.service';
import { UserService } from '../../api/services/user.service';

@Component({
  selector: 'app-user-tab-home',
  templateUrl: './user-tab-home.page.html',
  styleUrls: ['./user-tab-home.page.scss'],
})
export class UserTabHomePage implements OnInit {

  private tabTitle$: string;
  private userId: number;

  constructor(
    public loginService : LoginService,
    public userService : UserService,
    public router: Router
    ) { }

  ngOnInit() {
    this.userId = Number(sessionStorage.getItem("userId"));
    this.userService.getUserDetail(this.userId).subscribe(user => {
      this.tabTitle$ =  user.firstName + " " + user.lastName + " : " + user.role.label;
    });
  }

  logoutFromSession(){
    this.loginService.logout();
  }

  /**
   * Function related to the report edition between two dates button
   */
  editReportBetweenTwoDates(){
    sessionStorage.setItem('selectedUserIdForComponent', String(this.userId));
    sessionStorage.setItem('insertCreator', 'fromUser');
    this.router.navigateByUrl('edit-report');
  }

  /**
   * Function related to the report edition button
   */
  editMonthReport(){
    this.userService.editMonthReport(this.userId);
  }
}
