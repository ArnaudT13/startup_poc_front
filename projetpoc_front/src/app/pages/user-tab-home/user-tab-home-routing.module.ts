import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserTabHomePage } from './user-tab-home.page';

const routes: Routes = [
  {
    path: '',
    component: UserTabHomePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserTabHomePageRoutingModule {}
