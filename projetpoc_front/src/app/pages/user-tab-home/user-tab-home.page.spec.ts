import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UserTabHomePage } from './user-tab-home.page';

describe('UserTabHomePage', () => {
  let component: UserTabHomePage;
  let fixture: ComponentFixture<UserTabHomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserTabHomePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UserTabHomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
