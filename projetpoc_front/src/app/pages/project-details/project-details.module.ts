import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProjectDetailsPageRoutingModule } from './project-details-routing.module';

import { ProjectDetailsPage } from './project-details.page';
import { UserListComponent } from '../../components/lists/user-list/user-list.component';
import { MilestoneSelectComponent } from '../../components/selects/milestone-select/milestone-select.component';
import { LogsListComponent } from '../../components/lists/logs-list/logs-list.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProjectDetailsPageRoutingModule
  ],
  declarations: [ProjectDetailsPage, UserListComponent, MilestoneSelectComponent, LogsListComponent]
})
export class ProjectDetailsPageModule {}
