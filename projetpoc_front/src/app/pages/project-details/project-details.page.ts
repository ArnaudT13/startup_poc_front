import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ProjectService } from 'src/app/api/services/project.service';
import { UserService } from 'src/app/api/services/user.service';
import { LogsService } from 'src/app/api/services/logs.service';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.page.html',
  styleUrls: ['./project-details.page.scss'],
})
/**
 * Page allowing a logged user to see the details of a selected project.
 */
export class ProjectDetailsPage implements OnInit {

  // BEGIN: SESSION PARAMETERS
  private userId: number;
  // END: SESSION PARAMETERS


  // BEGIN: ATTRIBUTES RETRIEVED FROM SENDING PAGE
  private origin$: string;
  private projectId: number;
  private action: string; // value = "refresh" if we came from logDetails
  // END: ATTRIBUTES RETRIEVED FROM SENDING PAGE


  // BEGIN: CONSTANTS
  private ACTION_CREATE_LOG = "create-log";
  private ACTION_EDIT_LOG = "edit-log";
  // END: CONSANTS

  private roleId: number;
  private tabTitle$: string; // Display the firstName, lastName and roleName of the logged user.
  private projectTitle$: string;
  private projectLatestMilestone$: string; // Display the name of latest Milestone of the selected project.
  private userList$: Array<any>; 
  private milestones$: any
  private latestMilestoneId$: any;
  private logs$: any; // Contains the array of logs of the users according to the selected milestone.
  private currentMilsetoneId: number;
  private pageOpened: boolean;
  private usersIdOfLogs: Array<any>;
  private logsIdOfUsers: Array<any>;

  constructor(
    private router: Router,
    public userService: UserService,
    public projectService: ProjectService,
    public logsService: LogsService
  ) { 
    if (this.router.getCurrentNavigation().extras.state) {
      const state = this.router.getCurrentNavigation().extras.state;
      this.origin$ = state.origin ? JSON.parse(state.origin) : '';
      this.projectId = state.projectId ? JSON.parse(state.projectId) : '';
      this.action = state.action ? JSON.parse(state.action) : '';
    }
  }

  ngOnInit() {
    this.initializePage();
  }
  
  /**
   * Function called from the select milestone component when the milestone filter is changed to refresh the displayed logs.
   * @param milestoneId Selected milestone Id
   */
  public milestoneLogsChanged(milestoneId: number): void { 
    this.currentMilsetoneId = milestoneId;
    this.logsService.getLogsForPersonProject(this.projectId, this.userId).subscribe(logs => {
      this.logs$ = [];
      logs.forEach((log) => {
        this.logsService.getMilestoneByLogId(log.id).subscribe(milestone => {
          if(milestone.id == milestoneId){
            this.logs$.push(log);
          }
        });
      });
    });

  }

  /**
   * Function called when the user selected a log from the logs list component. 
   * @param logId Selected log Id.
   */
  async logSelected(logId: number) {
    this.router.navigate(['/log-details/'], {
      state: {
        action: JSON.stringify(this.ACTION_EDIT_LOG),
        origin: JSON.stringify('project-details'),
        projectId: JSON.stringify(this.projectId),
        logId: JSON.stringify(logId)
      }
    });
  }

  /**
   * Function called when the user click on the button to create a new log.
   */
  public createNewLog() {
    this.router.navigate(['/log-details/'], {
      state: {
        action: JSON.stringify(this.ACTION_CREATE_LOG),
        origin: JSON.stringify('project-details'),
        projectId: JSON.stringify(this.projectId),
        milestoneId: JSON.stringify(this.currentMilsetoneId)
      }
    });  
  }

  /**
   * Function called by ngOnInit to initialize the page data.
   */
  private initializePage() {
    this.userId = Number(sessionStorage.getItem("userId"));
    this.userList$ = [];
    this.userService.getUserDetail(this.userId).subscribe(user => {
      this.tabTitle$ =  user.firstName + " " + user.lastName + " : " + user.role.label;
      this.roleId = user.role.id;

      this.projectService.getProjectDetails(this.projectId).subscribe(project => {
        this.projectTitle$ =  project.name;
        this.projectLatestMilestone$ = project.milestones[project.milestones.length - 1].milestoneTitle.title;
        
        // Extraction of the ID of the most recent milestone
        this.latestMilestoneId$ = project.milestones[project.milestones.length - 1].id;
        this.currentMilsetoneId = this.latestMilestoneId$;
        this.milestones$ = project.milestones;
  
        this.logs$ = [];
        // For each user, we retrieve the log if it relates to the project
        this.userList$.forEach(user => {
          // Here extraction of the logs of the project written by the user.
          this.logsService.getLogsForPersonProject(this.projectId, user.id).subscribe(logs => {
            logs.forEach((log) => {
              this.logsService.getMilestoneByLogId(log.id).subscribe(milestone => {
                if(milestone.id == this.latestMilestoneId$){
                  this.logs$.push(log);
                }
              });
            });
          });
        });
  
      });

    });

    // Tells the userList (collegues) to refresh its data.
    this.userService.getUserProjectsSub(this.projectId);

  }
}
