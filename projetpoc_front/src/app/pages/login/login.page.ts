import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Observable, Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { LoginService } from '../../api/services/login.service';
import { UserService } from '../../api/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  private resultLoginOperation : Promise<Boolean>;

  credentials = {username: 'tristetemps', password: 'dambenois'};

  constructor(
    public router: Router,
    public loginService: LoginService,
    public userService: UserService,
    public toastCtrl: ToastController 
    ) { }

  ngOnInit() {
  }

  workOffline() {
    this.router.navigate(['/offline']);
  }

  /**
   * Use to manage the login operation
   * @returns
   */
  login() {
    this.loginService.authenticate(this.credentials,  () => {
      // Retrieve the user which is authenticated
      this.userService.getUserByUsername(this.credentials.username).subscribe(developer => {
        // Set the session user
        this.loginService.setSessionUser(developer);

        // Manager redirection of the user
        this.managerRoleRedirection(developer.role.id);
        sessionStorage.setItem('userId', developer.id);
      });
    }, this.toastCtrl);
    return false;
  }

  /**
   * Function used to redirect the user on the good page tabs
   * @param roleId The role id of the user
   */
  managerRoleRedirection(roleId){
    if(roleId == 1){
      sessionStorage.setItem('role', 'developer');
      this.router.navigateByUrl('user-tabs');
    }else if(roleId == 2){
      sessionStorage.setItem('role', 'admin');
      this.router.navigateByUrl('admin-tabs');
    }else if(roleId == 3){
      sessionStorage.setItem('role', 'manager');
      this.router.navigateByUrl('manager-tabs');
    }else{

    }
  }
}
