import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { UserService } from 'src/app/api/services/user.service';
import { ProjectService } from '../../api/services/project.service';
import * as moment from 'moment';

@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.page.html',
  styleUrls: ['./add-project.page.scss'],
})
/**
 * Page allowing a manager to create a project with a pre-determined Milestone.
 */
export class AddProjectPage implements OnInit {

  // BEGIN: SESSION PARAMETERS
  private userId: number;
  // END: SESSION PARAMETERS


  // BEGIN: PARAMETERS RETRIEVED FROM SENDING PAGE
  private origin$: string; // Contains the relative path of the sending page for back button.
  // END: PARAMETERS RETRIEVED FROM SENDING PAGE


  // BEGIN: PROJECT VALUES
  private projectName: any;
  private milestoneDescription: any;
  private milestoneStartDate: any;
  private milestoneEndDate: any;
  // END: PROJECT VALUES

  // BEGIN: CONSTANTS
  private NOTIFICATION_PROJECT_CREATED = "Created.";
  private NOTIFICATION_ERROR = "An error occured during creation."
  private NOTIFICATION_INPUT_ERRORS = "Input are missing...";
  // END: CONSTANTS

  private tabTitle$: string; // Display firstName lastName and roleName of the logged User.


  constructor(
    private router: Router,
    private toastController: ToastController,
    private projectService: ProjectService,
    private userService: UserService
  ) { 
    if (this.router.getCurrentNavigation().extras.state) {
      const state = this.router.getCurrentNavigation().extras.state;
      this.origin$ = state.origin ? JSON.parse(state.origin) : '';
    }
  }

  ngOnInit() {
    this.userId = Number(sessionStorage.getItem("userId"));
    this.userService.getUserDetail(this.userId).subscribe(user => {
      this.tabTitle$ =  user.firstName + " " + user.lastName + " : " + user.role.label;
    });
  }

  /**
   * Performs verification of inputs, if everything is okay, send a request through the API to create the Project.
   */
  private createProject() {
    if((this.projectName != undefined) && (this.milestoneDescription != undefined) && (this.milestoneStartDate != undefined) && (this.milestoneEndDate != undefined)){
      let result = this.projectService.createProject(this.projectName, this.milestoneDescription, moment(this.milestoneStartDate).format('YYYY-MM-DD'), moment(this.milestoneEndDate).format('YYYY-MM-DD'));
      result.then((response) => {
        if(response){
          let project;
          this.projectService.getProjectByName(this.projectName).subscribe(res => {
            console.log(res);
            project = res[0];
            let result2 = this.projectService.addContributorToProject(project.id, this.userId)
            if(result2){
              this.notification(this.NOTIFICATION_PROJECT_CREATED);
            } else {
              this.notification(this.NOTIFICATION_ERROR);
            }
          });
        } else {
          this.notification(this.NOTIFICATION_ERROR);
        }
      }).then(() => this.ngOnInit());

      this.leavePage();
    } else {
      this.notification(this.NOTIFICATION_INPUT_ERRORS);
    }
  }

  // Display a toast with the given parameter as a message.
  async notification(notificationMessage: string) {
    const toast = await this.toastController.create({
      message: notificationMessage,
      duration: 2000
    });
    toast.present();
  }

  private leavePage() {
    this.router.navigate([this.origin$], {
      state: {
        origin: JSON.stringify('/manager-tabs/manager-tab-projects')
      }
    });
  }
}
