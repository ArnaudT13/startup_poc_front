import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ManagerTabUsersDetailsPageRoutingModule } from './manager-tab-users-details-routing.module';

import { ManagerTabUsersDetailsPage } from './manager-tab-users-details.page';
import { EditReportComponent } from 'src/app/components/edit-report/edit-report.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ManagerTabUsersDetailsPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [ManagerTabUsersDetailsPage, EditReportComponent]
})
export class ManagerTabUsersDetailsPageModule {}
