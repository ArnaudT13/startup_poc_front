import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ManagerTabUsersDetailsPage } from './manager-tab-users-details.page';

const routes: Routes = [
  {
    path: '',
    component: ManagerTabUsersDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManagerTabUsersDetailsPageRoutingModule {}
