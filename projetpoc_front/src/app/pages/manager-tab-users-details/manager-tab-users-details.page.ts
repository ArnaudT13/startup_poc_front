import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonInfiniteScroll, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { UserService } from '../../api/services/user.service';

@Component({
  selector: 'app-manager-tab-users-details',
  templateUrl: './manager-tab-users-details.page.html',
  styleUrls: ['./manager-tab-users-details.page.scss'],
})
export class ManagerTabUsersDetailsPage implements OnInit {

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  
  private tabTitle$: string;
  private managerId: number;

  private selectedUserId: number;

  userDetail$: Observable<any>;

  userProjects$: Observable<any>;

  userLogs$: Observable<any>;

  constructor(
    public userService: UserService,
    public toastCtrl: ToastController,
    public router: Router
  ) { }

  ngOnInit() {
    this.managerId = Number(sessionStorage.getItem("userId"));
    this.userService.getUserDetail(this.managerId).subscribe(user => {
      this.tabTitle$ =  user.firstName + " " + user.lastName + " : " + user.role.label;
    });
    
    this.selectedUserId = Number(sessionStorage.getItem('userSelectedId'));

    this.userDetail$ = this.userService.getUserDetail(this.selectedUserId);

    this.userProjects$ = this.userService.getUserProjects(this.selectedUserId);

    this.userLogs$ = this.userService.getUserLogs(this.selectedUserId);
  }

  /**
   * Function related to the report edition between two dates button
   */
  editReportBetweenTwoDates(){
    sessionStorage.setItem('selectedUserIdForComponent', String(this.selectedUserId));
    sessionStorage.setItem('insertCreator', 'fromManager');
    this.router.navigateByUrl('edit-report');
  }

  /**
   * Function related to the report edition button
   */
  editMonthReport(){
    this.userService.editMonthReport(this.selectedUserId);
  }

  /**
   * Function used to diplay the toast of the report execution operation
   * @param status The status of the user adding operation
   */
  async reportExecutionToast(status: boolean) {
    let toast;
    if (status) {
      toast = await this.toastCtrl.create({
        message: 'Success : Report edition',
        color: 'success',
        translucent: true,
        duration: 3000
      });
    }else{
      toast = await this.toastCtrl.create({
        message: 'Error : Report edition',
        color: 'danger',
        translucent: true,
        duration: 3000
      });
    }

    toast.present();
  }

}
