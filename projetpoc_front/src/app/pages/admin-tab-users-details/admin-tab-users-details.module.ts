import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdminTabUsersDetailsPageRoutingModule } from './admin-tab-users-details-routing.module';

import { AdminTabUsersDetailsPage } from './admin-tab-users-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdminTabUsersDetailsPageRoutingModule
  ],
  declarations: [AdminTabUsersDetailsPage]
})
export class AdminTabUsersDetailsPageModule {}
