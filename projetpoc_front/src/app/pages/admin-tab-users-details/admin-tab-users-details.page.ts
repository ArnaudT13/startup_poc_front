import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { UserService } from '../../api/services/user.service';

@Component({
  selector: 'app-admin-tab-users-details',
  templateUrl: './admin-tab-users-details.page.html',
  styleUrls: ['./admin-tab-users-details.page.scss'],
})
export class AdminTabUsersDetailsPage implements OnInit {

  private adminId: number;
  private tabTitle$: string;
  
  private selectedUserId: number;

  // Response adding operation promise
  private responseAddingOperation: Promise<boolean>;

  // Response adding operation promise
  private responseChangingRoleOperation: Promise<boolean>;

  // Response adding operation promise
  private responseDeletingOperation: Promise<boolean>;

  userDetail$: Observable<any>;

  userProjects$: Observable<any>;

  userLogs$: Observable<any>;

  constructor(
    public userService: UserService,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public router: Router
  ) { }

  ngOnInit() {
    this.adminId = Number(sessionStorage.getItem("userId"));
    this.userService.getUserDetail(this.adminId).subscribe(user => {
      this.tabTitle$ =  user.firstName + " " + user.lastName + " : " + user.role.label;
    });

    this.selectedUserId = Number(sessionStorage.getItem('userSelectedId'));

    this.userDetail$ = this.userService.getUserDetail(this.selectedUserId);

    this.userProjects$ = this.userService.getUserProjects(this.selectedUserId);

    this.userLogs$ = this.userService.getUserLogs(this.selectedUserId);
  }

  /**
   * Function related to the role changement button
   */
  editRoleUser(){
    // Get roles
    let roleList$: Observable<any> = this.userService.getRoles();

    // Get user details
    let userDetail = this.userService.getUserDetail(this.selectedUserId);

    // Create input dictionary for select input
    let inputRoles={roles:[]};

    // Crete select input values
    userDetail.subscribe( user => {
      roleList$.subscribe(
        roles => {
          // For each role, add it to the dictionary
          roles.forEach((role) => {
            if(user.role.id !== role.id){
              inputRoles.roles.push({name: "radio" + role.id, type: "radio", label: role.label, value: role.id});
            }
          });
          
        // Call alert window
        this.editRoleUserAlertWindows(inputRoles);
      });
    });
  }

  /**
   * Sub-function used to diplay the role selection windows
   * @param inputRoles The dictionnary for the radio input
   */
  async editRoleUserAlertWindows(inputRoles){
    // Create alert windows which contains the radio input
    const alert = await this.alertCtrl.create({
      header: 'Change role',
      inputs: inputRoles.roles,
      buttons: [{
          text: 'Cancel',
          role: 'cancel'
        }, {
          text: 'Ok',
          handler: async (roleId) => {
            // Call the http patch
            this.responseChangingRoleOperation = this.userService.changeRoleForUser(this.selectedUserId, roleId);
            
            // Display toast operation and refresh page
            this.responseChangingRoleOperation
              .then((response) => {
                  this.userService.manageSuccessErrorToast(this.toastCtrl, response, "Role changed", "Role note changed")
                })
              .then(() => this.ngOnInit());
          }
      }
    ]});
    await alert.present();
  }

  /**
  * Function related to the user deleting button
  */
  deleteUser(){
    this.responseDeletingOperation = this.userService.deleteUser(this.selectedUserId);

    this.responseDeletingOperation
      .then((response) => {
          this.userService.manageSuccessErrorToast(this.toastCtrl, response, "User deleted", "User not deleted")
        });

    this.router.navigateByUrl('admin-tabs/admin-tab-users').then(() =>
      window.location.reload()
    );
  }


  /**
   * Function related to the change manager button
   */
  addOrChangeUserManager(){
    // Get users which not belongs to the manager
    let userListToSelectObservable$: Observable<any> = this.userService.getUsers();

    // Get user details
    let userDetail = this.userService.getUserDetail(this.selectedUserId);

    // Create input dictionary for select input
    let inputDeveloper={developers:[]};

    // Crete select input values
    userDetail.subscribe( user => {
      // If the user is an admin or a manager
      if (user.role.id === 3 || user.role.id === 2){
          this.userService.manageInfoToast(this.toastCtrl, "No manager for admin or manager");
      } else{
        userListToSelectObservable$.subscribe(
          users => {
            // For each developer, add it to the dictionary
            users.forEach((manager) => {
              // If the user has no manager
              if((user.manager === null || user.manager.id !== manager.id) && manager.role.id === 3){
                inputDeveloper.developers.push({name: "radio" + manager.id, type: "radio", label: manager.firstName + " " + manager.lastName, value: manager.id});
              } 
            });
            
          // Call alert window
          this.addUserToTheManagerAlertWindows(inputDeveloper);
        });
      } 
    });
  }

  /**
   * Sub-function used to diplay the user add selection windows
   * @param inputDeveloper The dictionnary for the radio input
   */
  async addUserToTheManagerAlertWindows(inputDeveloper){
    if(inputDeveloper.developers.length === 0)
    {
      await this.userService.manageInfoToast(this.toastCtrl, "No managers available");
    } else{

      // Create alert windows which contains the radio input
      const alert = await this.alertCtrl.create({
        header: 'Change manager',
        inputs: inputDeveloper.developers,
        buttons: [{
            text: 'Cancel',
            role: 'cancel'
          }, {
            text: 'Ok',
            handler: async (managerId) => {
              // Call the http patch
              this.responseAddingOperation = this.userService.addOrChangeManagerForUser(this.selectedUserId, managerId);
              
              // Display toast operation and refresh page
              this.responseAddingOperation
                .then((response) => {
                    this.userService.manageSuccessErrorToast(this.toastCtrl, response, "Manager changed", "Manager changed")
                  })
                .then(() => this.ngOnInit());
            }
        }
      ]});
      await alert.present();
    }
  }

}
