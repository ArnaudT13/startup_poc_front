import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminTabUsersDetailsPage } from './admin-tab-users-details.page';

const routes: Routes = [
  {
    path: '',
    component: AdminTabUsersDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminTabUsersDetailsPageRoutingModule {}
