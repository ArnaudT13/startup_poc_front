import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-offline',
  templateUrl: './offline.page.html',
  styleUrls: ['./offline.page.scss'],
})
/**
 * Page which was supposed to allow the user to create logs offline.
 * TODO!
 */
export class OfflinePage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
