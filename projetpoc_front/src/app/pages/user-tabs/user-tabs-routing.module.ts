import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserTabsPage } from './user-tabs.page';

const routes: Routes = [
  {
    path: '',
    component: UserTabsPage,
    children: [
      {
        path: 'user-tab-home',
        loadChildren: () => import('../user-tab-home/user-tab-home.module').then(m => m.UserTabHomePageModule)
      },
      {
        path: 'user-tab-projects',
        loadChildren: () => import('../user-tab-projects/user-tab-projects.module').then(m => m.UserTabProjectsPageModule)
      },
      {
        path: '',
        redirectTo: '/user-tabs/user-tab-home',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/user-tabs/user-tab-home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserTabsPageRoutingModule {}
