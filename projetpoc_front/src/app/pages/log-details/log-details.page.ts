import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../api/services/user.service';
import { ProjectService } from '../../api/services/project.service';
import { LogsService } from '../../api/services/logs.service';
import { MilestoneService } from '../../api/services/milestone.service';

import { ToastController } from '@ionic/angular';
import * as moment from 'moment';

@Component({
  selector: 'app-log-details',
  templateUrl: './log-details.page.html',
  styleUrls: ['./log-details.page.scss'],
})
/**
 * Page allowing the logged user to create a log.
 */
export class LogDetailsPage implements OnInit {

  // BEGIN: SESSION PARAMETERS
  private userId: number;
  // END: SESSION PARAMETERS


  // BEGIN: ATTRIBUTES RETRIEVED FROM SENDING PAGE
  private action: string; // Indicates if the user wants to create or edit an existing log.
  private origin$: string; // Contains the relative path of the sending page.
  private projectId: number;
  private logId: number;
  private milestoneId: number; // Only if action == "create-log"
  // END: ATTRIBUTES RETRIEVED FROM SENDING PAGE


  // BEGIN: LABELS AND LOG VALUES
  private labelDeveloper$: string;
  private labelDeveloperName$: string;
  private logDate$: string;
  private logDescription$: string;
  private logGranularity$: number;

  private inputLogDate$: any;
  private inputLogDescription$: any;
  private inputLogGranularity$: any;
  // END: LABELS AND LOG VALUES


  // BEGIN: CONSTANTS
  private NOTIFICATION_NO_CHANGES = "No changes.";
  private NOTIFICATION_CHANGES_SAVED = "Saved.";
  private NOTIFICATION_CHANGES_ERROR = "An error occured...";
  private NOTIFICATION_INPUT_ERRORS = "Input are missing...";
  private NOTIFICATION_IMPOSSIBLE_DELETE_LOGS = "Can't delete a log which doesn't exist!";
  private NOTIFICATION_ERROR_IN_GRANULARITY = "Granularity must be in greater than 0!";
  // END: CONSTANTS


  private roleId: number; // Role of the user to allow or forbid actions.
  private tabTitle$: string; // Display firstName, lastName and roleName of the logged user.
  private cardTitle$: string; // Display the project name and the milestoneTitle name.
  private cardSubTitle$: string; // Display the milestone description and its dates.


  constructor(
    private router: Router,
    private userService: UserService,
    private projectService: ProjectService,
    private logsService: LogsService,
    private milestoneService: MilestoneService,
    public toastController: ToastController
  ) { 
    if (this.router.getCurrentNavigation().extras.state) {
      const state = this.router.getCurrentNavigation().extras.state;
      this.action = state.action ? JSON.parse(state.action) : '';
      this.origin$ = state.origin ? JSON.parse(state.origin) : '';
      this.projectId = state.projectId ? JSON.parse(state.projectId) : '';

      if(this.action == "create-log"){
        this.milestoneId = state.milestoneId ? JSON.parse(state.milestoneId) : '';
      } else {
        if (this.action == "edit-log") {
          this.logId = state.logId ? JSON.parse(state.logId) : '';
        } else {
          console.log("Case impossible!")
        }
      }
    }
  }

  ngOnInit() {
    // We query the API to get data to display various information.
    this.userId = Number(sessionStorage.getItem("userId"));
    this.userService.getUserDetail(this.userId).subscribe(user => {
      this.tabTitle$ =  user.firstName + " " + user.lastName + " : " + user.role.label;
      this.roleId = user.role.id;
      if(this.roleId === 1) { // Developer
        this.labelDeveloper$ = "";
        this.labelDeveloperName$ = "";
      } else {
        if(this.roleId === 3) { // Manager
          this.labelDeveloper$ = "Person : "; // TODO Future improovement.
          this.labelDeveloperName$ = "";
        } else {
          console.log("Impossible Case!");
        }
      }
    });
    this.projectService.getProjectDetails(this.projectId).subscribe(project => {
      this.cardTitle$ =  project.name;
    });

    if(this.action === "create-log"){ // TODO CHANGE TO CONSTANT
      this.milestoneService.getMilestoneByMilestoneId(this.milestoneId).subscribe(milestone => {
        this.cardTitle$ = this.cardTitle$ + " - " + milestone.milestoneTitle.title;
        this.cardSubTitle$ = milestone.description + " from " + milestone.startDate + " to " + milestone.endDate;
      });
    } else {
      if (this.action === "edit-log") {
        this.logsService.getMilestoneByLogId(this.logId).subscribe(milestone => {
          this.cardTitle$ = this.cardTitle$ + " - " + milestone.milestoneTitle.title;
          this.cardSubTitle$ = milestone.description + " from " + milestone.startDate + " to " + milestone.endDate;
        });
        this.logsService.getLogById(this.logId).subscribe(log => {
          this.logDate$ = log.editDate;
          this.inputLogDate$ = this.logDate$;
          this.logDescription$ = log.description;
          this.logGranularity$ = log.granularity;
        });
      } else {
        console.log("Case impossible!");
      }
    }
  }

  /**
   * Called when the user clicks on the buttons to save/create its log.
   * Call the API to create or edit the log according to the previous page choice.
   * Performs a verification of the fields.
   */
  private saveLog() {
    if(this.action === "edit-log"){
      if((this.logDate$ === this.inputLogDate$) && (this.logDescription$ === this.inputLogDescription$) && (this.logGranularity$ === this.inputLogGranularity$)){
        // No changes
        this.notification(this.NOTIFICATION_NO_CHANGES);
      } else {
        if(this.inputLogDescription$.trim().length > 0){
          if(this.inputLogGranularity$ > 0){
              let status = this.logsService.updateLog(this.logId, this.inputLogDate$, this.inputLogGranularity$, this.inputLogDescription$);
              if(status){
                this.notification(this.NOTIFICATION_CHANGES_SAVED);
              } else {
                this.notification(this.NOTIFICATION_CHANGES_ERROR);
              }
              this.leavePage();
          } else {
              this.notification(this.NOTIFICATION_ERROR_IN_GRANULARITY);
          }
        } else {
          this.notification(this.NOTIFICATION_INPUT_ERRORS);
        }
      }
    } else {
      if(this.action === "create-log"){
        if((this.inputLogDate$ != undefined) && (this.inputLogDescription$ != undefined) && (this.inputLogGranularity$ != undefined)){
          if(this.inputLogDescription$.trim().length > 0){
            if(this.inputLogGranularity$ > 0){
              this.logsService.addLog(moment(this.inputLogDate$).format('YYYY-MM-DD'), this.inputLogGranularity$, this.inputLogDescription$, this.milestoneId, this.userId);
              this.leavePage();
            } else {
              this.notification(this.NOTIFICATION_ERROR_IN_GRANULARITY);
            }
          } else {
            this.notification(this.NOTIFICATION_INPUT_ERRORS);
          }
                 
        } else {
          this.notification(this.NOTIFICATION_INPUT_ERRORS);
        }
      } else {
        console.log("Impossible Case!")
      }
    }
  }

  /**
   * Works only if the action chosen in the previous page was to edit a log.
   * Send a query to the API to delete the log.
   */
  private deleteLog() {
    if(this.action === "edit-log") {
      let status = this.logsService.deleteLog(this.logId);
      /*
      if(status){
        this.notification(this.NOTIFICATION_CHANGES_SAVED);
      } else {
        this.notification(this.NOTIFICATION_CHANGES_ERROR);
      }*/
      this.leavePage();
    } else {
      this.notification(this.NOTIFICATION_IMPOSSIBLE_DELETE_LOGS);
    }
  }

  private leavePage() {
    this.router.navigate([this.origin$], {
      state: {
        origin: JSON.stringify('user-tabs/user-tab-projects'),
        projectId: JSON.stringify(this.projectId)
      }
    });
  }

  /**
   * Display a notification in a Toast acording to the specified parameters
   * @param notificationMessage Message to display in a Toast
   */
  async notification(notificationMessage: string) {
    const toast = await this.toastController.create({
      message: notificationMessage,
      duration: 2000
    });
    toast.present();
  }
}
