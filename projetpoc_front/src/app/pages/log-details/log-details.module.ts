import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LogDetailsPageRoutingModule } from './log-details-routing.module';

import { LogDetailsPage } from './log-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LogDetailsPageRoutingModule
  ],
  declarations: [LogDetailsPage]
})
export class LogDetailsPageModule {}
