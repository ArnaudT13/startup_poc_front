import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminTabsPage } from './admin-tabs.page';

const routes: Routes = [
  {
    path: '',
    component: AdminTabsPage,
    children: [
      {
        path: 'admin-tab-home',
        loadChildren: () => import('../admin-tab-home/admin-tab-home.module').then(m => m.AdminTabHomePageModule)
      },
      {
        path: 'admin-tab-users',
        loadChildren: () => import('../admin-tab-users/admin-tab-users.module').then(m => m.AdminTabUsersPageModule)
      },
      {
        path: '',
        redirectTo: '/admin-tabs/admin-tab-home',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/admin-tabs/admin-tab-home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminTabsPageRoutingModule {}
