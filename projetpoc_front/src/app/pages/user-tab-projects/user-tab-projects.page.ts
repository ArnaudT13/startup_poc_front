import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from 'src/app/api/services/user.service';
import { ProjectService } from 'src/app/api/services/project.service';

@Component({
  selector: 'app-user-tab-projects',
  templateUrl: './user-tab-projects.page.html',
  styleUrls: ['./user-tab-projects.page.scss'],
})
/**
 * Page which shows to the user its project.
 */
export class UserTabProjectsPage implements OnInit {

  // BEGIN: SESSION PARAMETERS
  private userId: number;
  // END: SESSION PARAMETERS

  private projectList$: Observable<any>;
  private tabTitle$: string; // Displaying the firstName, lastName and roleName of the loggedUser.
  private action: string;
  
  constructor(
    private router: Router,
    public userService: UserService,
    public projectService: ProjectService
  ) {  }

  /**
   * Method called when the user clicked on a project displayed in the projectList component.
   * @param id Selected Project Id
   */
  async selectedProject(id: number) {
    this.router.navigate(['/project-details/'], {
      state: {
        origin: JSON.stringify('user-tabs/user-tab-projects'),
        projectId: JSON.stringify(id)
      }
    });
  }

  /**
   * Initialise the page data and view.
   */
  ngOnInit() {
    this.userId = Number(sessionStorage.getItem("userId"));
    this.userService.getUserDetail(this.userId).subscribe(user => {
      this.tabTitle$ =  user.firstName + " " + user.lastName + " : " + user.role.label;
  });
    this.projectList$ = this.projectService.getUserProjectList(this.userId);
  }

}
