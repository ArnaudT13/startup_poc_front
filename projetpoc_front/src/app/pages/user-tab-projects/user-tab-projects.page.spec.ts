import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UserTabProjectsPage } from './user-tab-projects.page';

describe('UserTabProjectsPage', () => {
  let component: UserTabProjectsPage;
  let fixture: ComponentFixture<UserTabProjectsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserTabProjectsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UserTabProjectsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
