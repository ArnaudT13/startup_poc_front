import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UserTabProjectsPageRoutingModule } from './user-tab-projects-routing.module';

import { UserTabProjectsPage } from './user-tab-projects.page';
import { ProjectListComponent } from 'src/app/components/lists/project-list/project-list.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UserTabProjectsPageRoutingModule
  ],
  declarations: [UserTabProjectsPage, ProjectListComponent]
})
export class UserTabProjectsPageModule {}
