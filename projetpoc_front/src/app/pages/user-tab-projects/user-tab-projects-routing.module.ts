import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserTabProjectsPage } from './user-tab-projects.page';

const routes: Routes = [
  {
    path: '',
    component: UserTabProjectsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserTabProjectsPageRoutingModule {}
