import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ManagerTabsPage } from './manager-tabs.page';

const routes: Routes = [
  {
    path: '',
    component: ManagerTabsPage,
    children: [
      {
        path: 'manager-tab-home',
        loadChildren: () => import('../manager-tab-home/manager-tab-home.module').then(m => m.ManagerTabHomePageModule)
      },
      {
        path: 'manager-tab-projects',
        loadChildren: () => import('../manager-tab-projects/manager-tab-projects.module').then(m => m.ManagerTabProjectsPageModule)
      },
      {
        path: 'manager-tab-users',
        loadChildren: () => import('../manager-tab-users/manager-tab-users.module').then(m => m.ManagerTabUsersPageModule)
      },
      {
        path: '',
        redirectTo: '/manager-tabs/manager-tab-home',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/manager-tabs/manager-tab-home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManagerTabsPageRoutingModule {}
