import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ManagerTabsPageRoutingModule } from './manager-tabs-routing.module';

import { ManagerTabsPage } from './manager-tabs.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ManagerTabsPageRoutingModule
  ],
  declarations: [ManagerTabsPage]
})
export class ManagerTabsPageModule {}
