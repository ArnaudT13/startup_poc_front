import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ManagerTabProjectsPage } from './manager-tab-projects.page';

const routes: Routes = [
  {
    path: '',
    component: ManagerTabProjectsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManagerTabProjectsPageRoutingModule {}
