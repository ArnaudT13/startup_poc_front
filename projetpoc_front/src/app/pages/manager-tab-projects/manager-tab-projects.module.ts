import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ManagerTabProjectsPageRoutingModule } from './manager-tab-projects-routing.module';

import { ManagerTabProjectsPage } from './manager-tab-projects.page';
import { ProjectListComponent } from 'src/app/components/lists/project-list/project-list.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ManagerTabProjectsPageRoutingModule
  ],
  declarations: [ManagerTabProjectsPage, ProjectListComponent]
})
export class ManagerTabProjectsPageModule {}
