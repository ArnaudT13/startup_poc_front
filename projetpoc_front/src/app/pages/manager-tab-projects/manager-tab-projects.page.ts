import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { LoginService } from '../../api/services/login.service';
import { ProjectService } from 'src/app/api/services/project.service';
import { Project } from 'src/app/api/interfaces/project.interface'
import { UserService } from '../../api/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-manager-tab-projects',
  templateUrl: './manager-tab-projects.page.html',
  styleUrls: ['./manager-tab-projects.page.scss'],
})
/**
 * Allows a manger to see his projects and to go to the page to create a new one.
 */
export class ManagerTabProjectsPage implements OnInit {

  // BEGIN: SESSION PARAMETERS
  private managerId : number;
  // END: SESSION PARAMETERS

  private tabTitle$ : string; // Display the firstName, lastName and roleName of the loggedUser.
  private projectList$: Observable<any>; // Observable list containing the manager's projects.

  constructor(
    public router : Router,
    public projectService: ProjectService,
    public loginService : LoginService,
    public userService : UserService
  ) { }

  /**
   * Called when the user clicks on the button to create a Project.
   * Performs the navigation to the create project page.
   */
  private createProject() {
    this.router.navigate(['/add-project/'], {
      state: {
        origin: JSON.stringify('manager-tabs/manager-tab-projects'),
      }
    });
  }

  /**
   * Method called by the component when the user clicks on a project.
   * Opens the page to view the project details.
   * @param id Id of the project to open the details.
   */
  async selectedProject(id: number) {
    this.router.navigate(['/project-details/'], {
      state: {
        origin: JSON.stringify('user-tabs/user-tab-projects'),
        projectId: JSON.stringify(id)
      }
    });
  }

  ngOnInit() {
    this.managerId = Number(sessionStorage.getItem("userId"));
    this.userService.getUserDetail(this.managerId).subscribe(user => {
      this.tabTitle$ =  user.firstName + " " + user.lastName + " : " + user.role.label;
    });
    this.projectList$ = this.projectService.getUserProjectList(this.managerId);
  }

}
