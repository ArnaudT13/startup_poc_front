import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ManagerTabHomePage } from './manager-tab-home.page';

describe('ManagerTabHomePage', () => {
  let component: ManagerTabHomePage;
  let fixture: ComponentFixture<ManagerTabHomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagerTabHomePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ManagerTabHomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
