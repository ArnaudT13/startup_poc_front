import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../api/services/login.service';
import { UserService } from '../../api/services/user.service';

@Component({
  selector: 'app-manager-tab-home',
  templateUrl: './manager-tab-home.page.html',
  styleUrls: ['./manager-tab-home.page.scss'],
})
export class ManagerTabHomePage implements OnInit {

  private managerId: number;
  private tabTitle$: string;

  constructor(
    public loginService : LoginService,
    public userService : UserService
    ) { }

  ngOnInit() {
    this.managerId = Number(sessionStorage.getItem("userId"));
    this.userService.getUserDetail(this.managerId).subscribe(user => {
      this.tabTitle$ =  user.firstName + " " + user.lastName + " : " + user.role.label;
    });
  }

  logoutFromSession(){
    this.loginService.logout();
  }

}
