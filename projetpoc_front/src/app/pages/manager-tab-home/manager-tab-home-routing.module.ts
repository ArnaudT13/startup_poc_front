import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ManagerTabHomePage } from './manager-tab-home.page';

const routes: Routes = [
  {
    path: '',
    component: ManagerTabHomePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManagerTabHomePageRoutingModule {}
