import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ManagerTabHomePageRoutingModule } from './manager-tab-home-routing.module';

import { ManagerTabHomePage } from './manager-tab-home.page';
import { HomeDetailsComponent } from 'src/app/components/home-details/home-details.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ManagerTabHomePageRoutingModule
  ],
  declarations: [ManagerTabHomePage, HomeDetailsComponent]
})
export class ManagerTabHomePageModule {}
