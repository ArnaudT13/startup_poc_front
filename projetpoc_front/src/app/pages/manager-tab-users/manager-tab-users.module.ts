import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ManagerTabUsersPageRoutingModule } from './manager-tab-users-routing.module';

import { ManagerTabUsersPage } from './manager-tab-users.page';
import { UserListComponent } from '../../components/lists/user-list/user-list.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ManagerTabUsersPageRoutingModule
  ],
  declarations: [ManagerTabUsersPage, UserListComponent]
})
export class ManagerTabUsersPageModule {}
