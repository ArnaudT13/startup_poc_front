import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, ToastController } from '@ionic/angular';
import {Observable, Subscription} from 'rxjs';
import { LoginService } from '../../api/services/login.service';
import { UserService } from '../../api/services/user.service';

@Component({
  selector: 'app-manager-tab-users',
  templateUrl: './manager-tab-users.page.html',
  styleUrls: ['./manager-tab-users.page.scss'],
})
export class ManagerTabUsersPage implements OnInit {

  private tabTitle$: string;
  private managerId: number;

  // Response adding operation promise
  private responseAddingOperation: Promise<boolean>;
  private responseDeletingOperation: Promise<boolean>;

  // User list of the page
  userList$: any[];

  constructor(
    public router: Router,
    public userService: UserService,
    public loginService: LoginService,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController
  ) { }

  ngOnInit() {
    this.managerId = Number(sessionStorage.getItem("userId"));
    this.userService.getUserDetail(this.managerId).subscribe(user => {
      this.tabTitle$ =  user.firstName + " " + user.lastName + " : " + user.role.label;
    });
    this.userService.getUsersForManagerSub(this.managerId);
    this.userService.userListSubject.subscribe(
        (users : any[])=>{
          this.userList$ = users;
        }
    );
    this.userService.emitUserListSubject();
  }

  /**
   * Select a user in the list and route to the user-datails page
   * @param id The user id
   */
  async selectedUser(id: number) {
    sessionStorage.setItem('userSelectedId', String(id));
    await this.router.navigateByUrl('manager-tab-users-details');
  }


  /**
   * Function related to the add user to the manager button
   */
  async addUserToTheManager(){
    // Get users which not belongs to the manager
    let userListToSelectObservable$: Observable<any> = this.userService.getUsers();

    // Create input dictionary for select input
    let inputDeveloper={developers:[]};

    // Crete select input values
    userListToSelectObservable$.subscribe(
        developers => {
          // For each developer, add it to the dictionary
          developers.forEach((developer) => {
            // If the developer is available and not belongs to the manager
            if((developer.manager === null || developer.manager.id !== this.managerId) && developer.role.id !== 3){
              inputDeveloper.developers.push({name: "radio" + developer.id, type: "radio", label: developer.firstName + " " + developer.lastName, value: developer.id});
            } 
          });
          
        // Call alert window
        this.addUserToTheManagerAlertWindows(inputDeveloper);
    });
  }

  /**
   * Sub-function used to diplay the user add selection windows
   * @param inputDeveloper The dictionnary for the radio input
   */
  async addUserToTheManagerAlertWindows(inputDeveloper){
    if(inputDeveloper.developers.length === 0)
    {
      await this.userService.manageInfoToast(this.toastCtrl, "No users available");
    } else{

      // Create alert windows which contains the radio input
      const alert = await this.alertCtrl.create({
        header: 'Add developer',
        inputs: inputDeveloper.developers,
        buttons: [{
            text: 'Cancel',
            role: 'cancel'
          }, {
            text: 'Ok',
            handler: async (userId) => {
              // Call the http patch
              this.responseAddingOperation = this.userService.addOrChangeManagerForUser(userId, this.managerId);
              
              // Display toast operation and refresh page
              this.responseAddingOperation
                .then((response) => {
                    this.userService.manageSuccessErrorToast(this.toastCtrl, response, "User added", "User not added")
                  })
                .then(() => this.ngOnInit());
            }
        }
      ]});
      await alert.present();
    }
  }

  /**
   * Function related to the delete user to the manager button
   */
  async deleteUserToTheManager(){
    // Get users
    this.userService.getUsersForManagerSub(this.managerId);
    // Create input dictionary for select input
    let inputDeveloper={developers:[]};

    // Developer list displayed
    // For each developer, add it to the dictionary
    this.userList$.forEach((developer) => {
            inputDeveloper.developers.push({name: "radio" + developer.id, type: "radio", label: developer.firstName + " " + developer.lastName, value: developer.id});
        });
    // Call alert window
    this.deleteUserToTheManagerAlertWindows(inputDeveloper);
  }

  /**
   * Sub-function used to diplay the user delete selection windows
   * @param inputDeveloper The dictionnary for the radio input
   */
  async deleteUserToTheManagerAlertWindows(inputDeveloper){
    if(inputDeveloper.developers.length === 0)
    {
      await this.userService.manageInfoToast(this.toastCtrl, "No users to delete");
    } else{

      // Create alert windows which contains the radio input
      const alert = await this.alertCtrl.create({
        header: 'Delete developer',
        inputs: inputDeveloper.developers,
        buttons: [{
            text: 'Cancel',
            role: 'cancel'
          }, {
            text: 'Ok',
            handler: async (userId) => {
              // Call the http patch
              this.responseDeletingOperation = this.userService.deleteUserForManager(userId,this.managerId);
              
              // Display toast operation and refresh page
              this.responseDeletingOperation
                .then((response) => {
                    this.userService.manageSuccessErrorToast(this.toastCtrl, response, "User deleted", "User not deleted")
                  })
                .then(() => this.ngOnInit());
            }
        }
      ]});
      await alert.present();
    }
  }


}
