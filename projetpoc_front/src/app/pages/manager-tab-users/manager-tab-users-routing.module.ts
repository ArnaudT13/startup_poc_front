import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ManagerTabUsersPage } from './manager-tab-users.page';

const routes: Routes = [
  {
    path: '',
    component: ManagerTabUsersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManagerTabUsersPageRoutingModule {}
