import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from "@angular/router";
import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class IsSignedInGuard implements CanActivate{

    constructor(
        private router: Router
    ){}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const tokenExist = Boolean(localStorage.getItem("tokenExist"));

        if(!tokenExist === true){
            this.router.navigateByUrl('/login');
        }

        return tokenExist;
    }

}